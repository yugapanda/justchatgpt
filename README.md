## What is this ?
- This application only calls the OpenAI Chat API. Just Wrapper.


## How to Build ?
- install
  - Rust: 1.67
    - https://rustup.rs/
  - Node: 18
    - https://nodejs.org/
  - tauri-cli
    - `cargo install tauri-cli`
- setup
  - `npm i`
- build 
  - `cargo tauri build`

## How to Use ?
- set environment variables `OPENAI_API_KEY` with OpenAI API Key.
- (sign up https://platform.openai.com/signup and pay.)
- start app.

