import { ChatCompletionRequestMessage, ChatCompletionRequestMessageRoleEnum, Configuration, OpenAIApi } from "openai";

const convertMessages = (messages: string[], currentMessage: string): ChatCompletionRequestMessage[] => {
  const m = [...messages, currentMessage].map((x, i) => {
    if (i % 2 === 0) {
      return {
        role: "user" as ChatCompletionRequestMessageRoleEnum,
        content: x,
      }
    }
    return {
      role: "assistant" as ChatCompletionRequestMessageRoleEnum,
      content: x
    }
  });
  return [{ role: "system", content: "日本語で返答してください" }, ...m]
}


export const chat = async (messages: string[], currentMessage: string, apiKey: string) => {
  const configuration = new Configuration({
    apiKey
  });
  const api = new OpenAIApi(configuration);

  const mes = convertMessages(messages, currentMessage);

  console.log(mes);

  return api.createChatCompletion({
    model: "gpt-3.5-turbo",
    messages: mes
  });
}