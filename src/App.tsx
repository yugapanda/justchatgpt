import { useEffect, useState } from "react";
import "./App.css";
import { chat } from "./chat";
import ReactMarkdown from 'react-markdown';
import CodeBlock from "./CodeBlock";
import { invoke } from '@tauri-apps/api/tauri'


const App = () => {

  const [messages, setMessages] = useState<string[]>([]);
  const [message, setMessage] = useState<string>("");
  const [sending, setSending] = useState(false);
  const [apiKey, setApiKey] = useState<string>("");

  const getApiKey = async () => {
    const response: string = await invoke("get_api_key");
    setApiKey(response)
  }

  useEffect(() => {
    getApiKey();
  }, [])



  const sendChat = async (messages: string[], message: string) => {
    const prom = chat(messages, message, apiKey);
    setSending(true);
    const res = await prom;
    const response = res.data;
    const m = response.choices[0]?.message?.content ?? ""
    setMessages([...messages, message, m]);
    setMessage("");
    setSending(false);
  }

  const handleKeyDown = (
    event: React.KeyboardEvent<HTMLInputElement>,
    messages: string[],
    message: string) => {
    if (event.key === 'Enter' && (event.ctrlKey || event.metaKey)) {
      sendChat(messages, message);
    }
  }


  if(apiKey === "") {
    return (
      <p>
        Loading API Key or API Key Not Found
      </p>
    )
  }

  return (
    <div>
      <div style={{ height: "450px", overflow: "auto" }}>
        {
          messages.map((x, i) =>
            <p>
              {i % 2 === 1 ? "GPT: " : "USER: "}
              <ReactMarkdown
                components={{ code: CodeBlock }}
              >{x}</ReactMarkdown>
            </p>
          )
        }
      </div>
      <div style={{ position: "fixed", bottom: "0", width: "98%" }}>
        <div style={{ display: "flex", justifyContent: "center" }}>
          <input
            style={{ width: "100%" }}
            value={message}
            onChange={(x) => setMessage(x.currentTarget.value)}
            disabled={sending}
            onKeyDown={(e) => handleKeyDown(e, messages, message)}
          />
        </div>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <button onClick={() => setMessages([])}>クリア</button>
          <button onClick={() => navigator.clipboard.writeText(messages.join("\n"))}>コピー</button>
          <button
            style={{ right: 0 }}
            onClick={(x) => sendChat(messages, message)}>送信</button>
        </div>
      </div>
    </div>
  );
}

export default App;
